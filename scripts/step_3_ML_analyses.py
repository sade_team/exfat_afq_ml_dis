#!/usr/bin/env python3
# -*- coding: utf-8 -*-

git_directory = "/home/lsna/git/exfat_afq_ml_dis/"
main_dir = git_directory + 'afq_output'
figures_dir = git_directory + 'figures'

#%% DO NOT MODIFY BEYOND THIS POINT
__author__ = "Saul Pascual-Diaz, Federico Varriano"
__institution__ = "Laboratory of Surgical Neuroanatomy (LSNA)."
__date__ = "2020/06/24"
__version__ = "1.1"
__status__ = "paper_version"

#%% Libraries
import numpy as np
import os
from lsna_dependences import ML_class
  
#%% Loading data

AFQ_L_FA_3T = np.load(os.path.join(git_directory, 'afq_output', 'L_FA_3T.npy'))
AFQ_L_MD_3T = np.load(os.path.join(git_directory, 'afq_output', 'L_MD_3T.npy'))
AFQ_L_RD_3T = np.load(os.path.join(git_directory, 'afq_output', 'L_RD_3T.npy'))
AFQ_L_AD_3T = np.load(os.path.join(git_directory, 'afq_output', 'L_AD_3T.npy'))

AFQ_R_FA_3T = np.load(os.path.join(git_directory, 'afq_output', 'R_FA_3T.npy'))
AFQ_R_MD_3T = np.load(os.path.join(git_directory, 'afq_output', 'R_MD_3T.npy'))
AFQ_R_RD_3T = np.load(os.path.join(git_directory, 'afq_output', 'R_RD_3T.npy'))
AFQ_R_AD_3T = np.load(os.path.join(git_directory, 'afq_output', 'R_AD_3T.npy'))

AFQ_L_FA_7T = np.load(os.path.join(git_directory, 'afq_output', 'L_FA_7T.npy'))
AFQ_L_MD_7T = np.load(os.path.join(git_directory, 'afq_output', 'L_MD_7T.npy'))
AFQ_L_RD_7T = np.load(os.path.join(git_directory, 'afq_output', 'L_RD_7T.npy'))
AFQ_L_AD_7T = np.load(os.path.join(git_directory, 'afq_output', 'L_AD_7T.npy'))

AFQ_R_FA_7T = np.load(os.path.join(git_directory, 'afq_output', 'R_FA_7T.npy'))
AFQ_R_MD_7T = np.load(os.path.join(git_directory, 'afq_output', 'R_MD_7T.npy'))
AFQ_R_RD_7T = np.load(os.path.join(git_directory, 'afq_output', 'R_RD_7T.npy'))
AFQ_R_AD_7T = np.load(os.path.join(git_directory, 'afq_output', 'R_AD_7T.npy'))

#%% do work
y = [0]*120 + [1]*120
itera = 100
o = 0
d = 100

# Laterality X matrices
LAT_X_FA_3T = np.concatenate((AFQ_L_FA_3T[o:d], AFQ_R_FA_3T[o:d]), axis = 1)
LAT_X_MD_3T = np.concatenate((AFQ_L_MD_3T[o:d], AFQ_R_MD_3T[o:d]), axis = 1)
LAT_X_RD_3T = np.concatenate((AFQ_L_RD_3T[o:d], AFQ_R_RD_3T[o:d]), axis = 1)
LAT_X_AD_3T = np.concatenate((AFQ_L_AD_3T[o:d], AFQ_R_AD_3T[o:d]), axis = 1)
LAT_X_FA_7T = np.concatenate((AFQ_L_FA_7T[o:d], AFQ_R_FA_7T[o:d]), axis = 1)
LAT_X_MD_7T = np.concatenate((AFQ_L_MD_7T[o:d], AFQ_R_MD_7T[o:d]), axis = 1)
LAT_X_RD_7T = np.concatenate((AFQ_L_RD_7T[o:d], AFQ_R_RD_7T[o:d]), axis = 1)
LAT_X_AD_7T = np.concatenate((AFQ_L_AD_7T[o:d], AFQ_R_AD_7T[o:d]), axis = 1)

print('\nFA 3T LAT  ---------------------------------------------------------')
ML_class(LAT_X_FA_3T.T, y,
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_FA_3T') 
print('\nMD 3T LAT  ---------------------------------------------------------')
ML_class(LAT_X_MD_3T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_MD_3T') 
print('\nRD 3T LAT  ---------------------------------------------------------')
ML_class(LAT_X_RD_3T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_RD_3T') 
print('\nAD 3T LAT  ---------------------------------------------------------')
ML_class(LAT_X_AD_3T.T, y,
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_AD_3T') 
print('\nFA 7T LAT  ---------------------------------------------------------')
ML_class(LAT_X_FA_7T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_FA_7T')
print('\nMD 7T LAT  ---------------------------------------------------------')
ML_class(LAT_X_MD_7T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_MD_7T')
print('\nRD 7T LAT  ---------------------------------------------------------')
ML_class(LAT_X_RD_7T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_RD_7T')
print('\nAD 7T LAT  ---------------------------------------------------------')
ML_class(LAT_X_AD_7T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_AD_7T') 

#%% COMBINATED MODELS

print('\nCOMBINED MODEL IN 3T -----------------------------------------------')
L = np.concatenate((AFQ_L_FA_3T[o:d], AFQ_L_MD_3T[o:d], AFQ_L_RD_3T[o:d],
                    AFQ_L_AD_3T[o:d]), axis = 0)
R = np.concatenate((AFQ_R_FA_3T[o:d], AFQ_R_MD_3T[o:d], AFQ_R_RD_3T[o:d],
                    AFQ_R_AD_3T[o:d]), axis = 0)
X = np.concatenate((L, R), axis = 1)
ML_class(X.T, y, iters = itera, fname = 'LAT_X_COMB_3T')

print('\nCOMBINED MODEL IN 7T -----------------------------------------------')
L = np.concatenate((AFQ_L_FA_7T[o:d], AFQ_L_MD_7T[o:d], AFQ_L_RD_7T[o:d],
                    AFQ_L_AD_7T[o:d]), axis = 0)
R = np.concatenate((AFQ_R_FA_7T[o:d], AFQ_R_MD_7T[o:d], AFQ_R_RD_7T[o:d],
                    AFQ_R_AD_7T[o:d]), axis = 0)
X = np.concatenate((L, R), axis = 1)
ML_class(X.T, y, iters = itera, fname = 'LAT_X_COMB_7T')
ML_class(X.T, y, iters = itera, fname = 'LAT_X_COMB_7T')
