#!/usr/bin/env python3
# -*- coding: utf-8 -*-

git_directory = "/home/lsna/git/exfat_afq_ml_dis/"
main_dir = git_directory + 'afq_output'
figures_dir = git_directory + 'figures'

#%% DO NOT MODIFY BEYOND THIS POINT 
__author__ = "Saul Pascual-Diaz, Federico Varriano"
__institution__ = "Laboratory of Surgical Neuroanatomy (LSNA)."
__date__ = "2020/06/24"
__version__ = "1.1"
__status__ = "paper_version"
   
#%% Libraries
import numpy as np
import os
os.chdir(os.path.join(git_directory, 'scripts'))
from lsna_dependences import afq_figure

#%% LOADING DATA

## Variables
o = 0
d = 100
bonferroni = 8 + (d - o)*8

L_FA_3T = np.load(os.path.join(main_dir, 'L_FA_3T.npy'))
L_MD_3T = np.load(os.path.join(main_dir, 'L_MD_3T.npy'))
L_RD_3T = np.load(os.path.join(main_dir, 'L_RD_3T.npy'))
L_AD_3T = np.load(os.path.join(main_dir, 'L_AD_3T.npy'))

R_FA_3T = np.load(os.path.join(main_dir, 'R_FA_3T.npy'))
R_MD_3T = np.load(os.path.join(main_dir, 'R_MD_3T.npy'))
R_RD_3T = np.load(os.path.join(main_dir, 'R_RD_3T.npy'))
R_AD_3T = np.load(os.path.join(main_dir, 'R_AD_3T.npy'))

L_FA_7T = np.load(os.path.join(main_dir, 'L_FA_7T.npy'))
L_MD_7T = np.load(os.path.join(main_dir, 'L_MD_7T.npy'))
L_RD_7T = np.load(os.path.join(main_dir, 'L_RD_7T.npy'))
L_AD_7T = np.load(os.path.join(main_dir, 'L_AD_7T.npy'))

R_FA_7T = np.load(os.path.join(main_dir, 'R_FA_7T.npy'))
R_MD_7T = np.load(os.path.join(main_dir, 'R_MD_7T.npy'))
R_RD_7T = np.load(os.path.join(main_dir, 'R_RD_7T.npy'))
R_AD_7T = np.load(os.path.join(main_dir, 'R_AD_7T.npy'))

#%% LI ANALYSES

afq_figure(L_FA_3T[o:d], R_FA_3T[o:d],
       suptitle = 'Laterality comparison for the FA in 3T',
       output_fig = os.path.join(figures_dir, 'whole_sample_FA_3T.png'),
       bonferroni = bonferroni)
afq_figure(L_MD_3T[o:d], R_MD_3T[o:d],
       suptitle = 'Laterality comparison for the MD in 3T',
       output_fig = os.path.join(figures_dir, 'whole_sample_MD_3T.png'),
       bonferroni = bonferroni)
afq_figure(L_RD_3T[o:d], R_RD_3T[o:d],
       suptitle = 'Laterality comparison for the RD in 3T',
       output_fig = os.path.join(figures_dir, 'whole_sample_RD_3T.png'),
       bonferroni = bonferroni)
afq_figure(L_AD_3T[o:d], R_AD_3T[o:d],
       suptitle = 'Laterality comparison for the AD in 3T',
       output_fig = os.path.join(figures_dir, 'whole_sample_AD_3T.png'),
       bonferroni = bonferroni)
afq_figure(L_FA_7T[o:d], R_FA_7T[o:d],
       suptitle = 'Laterality comparison for the FA in 7T',
       output_fig = os.path.join(figures_dir, 'whole_sample_FA_7T.png'),
       bonferroni = bonferroni)
afq_figure(L_MD_7T[o:d], R_MD_7T[o:d],
       suptitle = 'Laterality comparison for the MD in 7T',
       output_fig = os.path.join(figures_dir, 'whole_sample_MD_7T.png'),
       bonferroni = bonferroni)
afq_figure(L_RD_7T[o:d], R_RD_7T[o:d],
       suptitle = 'Laterality comparison for the RD in 7T',
       output_fig = os.path.join(figures_dir, 'whole_sample_RD_7T.png'),
       bonferroni = bonferroni)
afq_figure(L_AD_7T[o:d], R_AD_7T[o:d],
       suptitle = 'Laterality comparison for the AD in 7T',
       output_fig = os.path.join(figures_dir, 'whole_sample_AD_7T.png'),
       bonferroni = bonferroni)