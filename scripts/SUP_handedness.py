#!/usr/bin/env python3
# -*- coding: utf-8 -*-

git_directory = "/home/lsna/git/exfat_afq_ml_dis/"
main_dir = '/home/lsna/data/DATA'
hcp_data = "/home/lsna/HCPDATA.csv"

#%% DO NOT MODIFY BEYOND THIS POINT
__author__ = "Saul Pascual-Diaz, Federico Varriano"
__institution__ = "Laboratory of Surgical Neuroanatomy (LSNA)."
__date__ = "2020/06/24"
__version__ = "1.1"
__status__ = "paper_version"

#%% Importing libraries
import pandas as pd
import os
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats

os.chdir(os.path.join(git_directory, 'scripts'))
from lsna_dependences import global_stat, afq_figure, ML_class

sns.set(style="whitegrid")
out_print = False

#%% Loading HCP data and test
hcp_df = pd.read_csv(hcp_data)

#%% Creating hcp indices for left-handed subjects
handedness = []
right_handed_subject = []
nonright_handed_subject = []
cnt = 0
for i, s in enumerate(sorted(os.listdir(main_dir))):
    if not os.path.isdir(os.path.join(main_dir, s)): continue
    if not os.path.isfile(os.path.join(main_dir, s, 'diffusion_3T', 'exFAT_L_10M.tck')): continue
    if cnt >= 120: continue
    
    hand = hcp_df[hcp_df.Subject == int(s)]['Handedness'].values[0]
    if out_print: print(f"{i}: subject {s}, Handedness: {hand}")
    handedness.append(hand)
    if hand >= 50:
        right_handed_subject.append(cnt)
    else:
        nonright_handed_subject.append(cnt)
    cnt += 1
    
#%% 01. Global Analyses for left-handed subjects

if not os.path.isfile(os.path.join(git_directory, 'global_output', 'L_FA_7T.npy')):
    L_FA_3T, R_FA_3T, L_MD_3T, R_MD_3T, L_AD_3T, R_AD_3T, L_RD_3T, R_RD_3T = global_stat(main_dir, field = '3T')
    L_FA_7T, R_FA_7T, L_MD_7T, R_MD_7T, L_AD_7T, R_AD_7T, L_RD_7T, R_RD_7T = global_stat(main_dir, field = '7T')
    np.save(os.path.join(git_directory, 'global_output', 'L_FA_3T.npy'), np.array(L_FA_3T[:120]))
    np.save(os.path.join(git_directory, 'global_output', 'R_FA_3T.npy'), np.array(R_FA_3T[:120]))
    np.save(os.path.join(git_directory, 'global_output', 'L_MD_3T.npy'), np.array(L_MD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_MD_3T.npy'), np.array(R_MD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'L_RD_3T.npy'), np.array(L_RD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_RD_3T.npy'), np.array(R_RD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'L_AD_3T.npy'), np.array(L_AD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_AD_3T.npy'), np.array(R_AD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'L_FA_7T.npy'), np.array(L_FA_7T[:120]))
    np.save(os.path.join(git_directory, 'global_output', 'R_FA_7T.npy'), np.array(R_FA_7T[:120]))
    np.save(os.path.join(git_directory, 'global_output', 'L_MD_7T.npy'), np.array(L_MD_7T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_MD_7T.npy'), np.array(R_MD_7T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'L_RD_7T.npy'), np.array(L_RD_7T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_RD_7T.npy'), np.array(R_RD_7T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'L_AD_7T.npy'), np.array(L_AD_7T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_AD_7T.npy'), np.array(R_AD_7T[:120])*1000)

L_FA_3T = np.load(os.path.join(git_directory, 'global_output', 'L_FA_3T.npy'))
R_FA_3T = np.load(os.path.join(git_directory, 'global_output', 'R_FA_3T.npy'))
L_MD_3T = np.load(os.path.join(git_directory, 'global_output', 'L_MD_3T.npy'))
R_MD_3T = np.load(os.path.join(git_directory, 'global_output', 'R_MD_3T.npy'))
L_RD_3T = np.load(os.path.join(git_directory, 'global_output', 'L_RD_3T.npy'))
R_RD_3T = np.load(os.path.join(git_directory, 'global_output', 'R_RD_3T.npy'))
L_AD_3T = np.load(os.path.join(git_directory, 'global_output', 'L_AD_3T.npy'))
R_AD_3T = np.load(os.path.join(git_directory, 'global_output', 'R_AD_3T.npy'))
L_FA_7T = np.load(os.path.join(git_directory, 'global_output', 'L_FA_7T.npy'))
R_FA_7T = np.load(os.path.join(git_directory, 'global_output', 'R_FA_7T.npy'))
L_MD_7T = np.load(os.path.join(git_directory, 'global_output', 'L_MD_7T.npy'))
R_MD_7T = np.load(os.path.join(git_directory, 'global_output', 'R_MD_7T.npy'))
L_RD_7T = np.load(os.path.join(git_directory, 'global_output', 'L_RD_7T.npy'))
R_RD_7T = np.load(os.path.join(git_directory, 'global_output', 'R_RD_7T.npy'))
L_AD_7T = np.load(os.path.join(git_directory, 'global_output', 'L_AD_7T.npy'))
R_AD_7T = np.load(os.path.join(git_directory, 'global_output', 'R_AD_7T.npy'))
    
#%% Creating dataframe for the right-handed sample

columns = ['Value', 'Field', 'Diffusion parameter', 'Hemisphere']
dataframe = pd.DataFrame(columns = columns)

for i in right_handed_subject:
    if out_print: print(f'Subject number {i} with FA = {L_FA_3T[i]}')
    data_list = []
    data_columns = []
    data_list.append(L_FA_3T[i])
    data_list.append('FA')
    data_list.append('3T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_FA_3T[i])
    data_list.append('FA')
    data_list.append('3T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_MD_3T[i])
    data_list.append('MD')
    data_list.append('3T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_MD_3T[i])
    data_list.append('MD')
    data_list.append('3T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_RD_3T[i])
    data_list.append('RD')
    data_list.append('3T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_RD_3T[i])
    data_list.append('RD')
    data_list.append('3T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_AD_3T[i])
    data_list.append('AD')
    data_list.append('3T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_AD_3T[i])
    data_list.append('AD')
    data_list.append('3T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_FA_7T[i])
    data_list.append('FA')
    data_list.append('7T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_FA_7T[i])
    data_list.append('FA')
    data_list.append('7T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_MD_7T[i])
    data_list.append('MD')
    data_list.append('7T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_MD_7T[i])
    data_list.append('MD')
    data_list.append('7T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_RD_7T[i])
    data_list.append('RD')
    data_list.append('7T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_RD_7T[i])
    data_list.append('RD')
    data_list.append('7T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_AD_7T[i])
    data_list.append('AD')
    data_list.append('7T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_AD_7T[i])
    data_list.append('AD')
    data_list.append('7T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False) 

#%% Plot
sns.catplot(x="Diffusion parameter", y="Value", hue="Hemisphere", col="Field", data=dataframe, kind="violin", split=True, height=8, aspect=.7, inner="quartile", palette="Set2");
plt.savefig('/home/spascual/git/paper_exfat_tractprofile/global_lat_comparison_right_handed.png', format = 'png')

#%% Stats params
p_values = np.zeros((8,1))
t_values = np.zeros((8,1))
o = 0
d = 100
bonferroni = 8 + (d - o)*8

#%% global stats

t_values[0], p_values[0] = stats.ttest_ind(L_FA_3T, R_FA_3T)
t_values[1], p_values[1] = stats.ttest_ind(L_MD_3T, R_MD_3T)
t_values[2], p_values[2] = stats.ttest_ind(L_RD_3T, R_RD_3T)
t_values[3], p_values[3] = stats.ttest_ind(L_AD_3T, R_AD_3T)
t_values[4], p_values[4] = stats.ttest_ind(L_FA_7T, R_FA_7T)
t_values[5], p_values[5] = stats.ttest_ind(L_MD_7T, R_MD_7T)
t_values[6], p_values[6] = stats.ttest_ind(L_RD_7T, R_RD_7T)
t_values[7], p_values[7] = stats.ttest_ind(L_AD_7T, R_AD_7T)
p_values = p_values*bonferroni

print('\nOUTPUT\n-------------')
print(p_values < 0.05)
print(p_values[p_values < 0.05])

#%% Loading AFQ right-handed subjects

AFQ_L_FA_3T = np.load(os.path.join(git_directory, 'afq_output', 'L_FA_3T.npy'))
AFQ_L_MD_3T = np.load(os.path.join(git_directory, 'afq_output', 'L_MD_3T.npy'))
AFQ_L_RD_3T = np.load(os.path.join(git_directory, 'afq_output', 'L_RD_3T.npy'))
AFQ_L_AD_3T = np.load(os.path.join(git_directory, 'afq_output', 'L_AD_3T.npy'))

AFQ_R_FA_3T = np.load(os.path.join(git_directory, 'afq_output', 'R_FA_3T.npy'))
AFQ_R_MD_3T = np.load(os.path.join(git_directory, 'afq_output', 'R_MD_3T.npy'))
AFQ_R_RD_3T = np.load(os.path.join(git_directory, 'afq_output', 'R_RD_3T.npy'))
AFQ_R_AD_3T = np.load(os.path.join(git_directory, 'afq_output', 'R_AD_3T.npy'))

AFQ_L_FA_7T = np.load(os.path.join(git_directory, 'afq_output', 'L_FA_7T.npy'))
AFQ_L_MD_7T = np.load(os.path.join(git_directory, 'afq_output', 'L_MD_7T.npy'))
AFQ_L_RD_7T = np.load(os.path.join(git_directory, 'afq_output', 'L_RD_7T.npy'))
AFQ_L_AD_7T = np.load(os.path.join(git_directory, 'afq_output', 'L_AD_7T.npy'))

AFQ_R_FA_7T = np.load(os.path.join(git_directory, 'afq_output', 'R_FA_7T.npy'))
AFQ_R_MD_7T = np.load(os.path.join(git_directory, 'afq_output', 'R_MD_7T.npy'))
AFQ_R_RD_7T = np.load(os.path.join(git_directory, 'afq_output', 'R_RD_7T.npy'))
AFQ_R_AD_7T = np.load(os.path.join(git_directory, 'afq_output', 'R_AD_7T.npy'))

study_group = range(120)
study_group_name = 'whole_sample'
AFQ_L_FA_3T = AFQ_L_FA_3T[:, study_group]
AFQ_L_MD_3T = AFQ_L_MD_3T[:, study_group]
AFQ_L_RD_3T = AFQ_L_RD_3T[:, study_group]
AFQ_L_AD_3T = AFQ_L_AD_3T[:, study_group]

AFQ_R_FA_3T = AFQ_R_FA_3T[:, study_group]
AFQ_R_MD_3T = AFQ_R_MD_3T[:, study_group]
AFQ_R_RD_3T = AFQ_R_RD_3T[:, study_group]
AFQ_R_AD_3T = AFQ_R_AD_3T[:, study_group]

AFQ_L_FA_7T = AFQ_L_FA_7T[:, study_group]
AFQ_L_MD_7T = AFQ_L_MD_7T[:, study_group]
AFQ_L_RD_7T = AFQ_L_RD_7T[:, study_group]
AFQ_L_AD_7T = AFQ_L_AD_7T[:, study_group]

AFQ_R_FA_7T = AFQ_R_FA_7T[:, study_group]
AFQ_R_MD_7T = AFQ_R_MD_7T[:, study_group]
AFQ_R_RD_7T = AFQ_R_RD_7T[:, study_group]
AFQ_R_AD_7T = AFQ_R_AD_7T[:, study_group]

#%% LI ANALYSES
## Variables

afq_figure(AFQ_L_FA_3T[o:d], AFQ_R_FA_3T[o:d],
           output_fig = os.path.join(git_directory, 'figures', f'{study_group_name}_FA_3T.png'),
           suptitle = 'Laterality comparison for the FA in 3T',
           bonferroni = bonferroni)

afq_figure(AFQ_L_MD_3T[o:d], AFQ_R_MD_3T[o:d],
           suptitle = 'Laterality comparison for the MD in 3T',
           output_fig = os.path.join(git_directory, 'figures', f'{study_group_name}_MD_3T.png'),
           bonferroni = bonferroni)
afq_figure(AFQ_L_RD_3T[o:d], AFQ_R_RD_3T[o:d],
           suptitle = 'Laterality comparison for the RD in 3T',
           output_fig = os.path.join(git_directory, 'figures', f'{study_group_name}_RD_3T.png'),
           bonferroni = bonferroni)
afq_figure(AFQ_L_AD_3T[o:d], AFQ_R_AD_3T[o:d],
           suptitle = 'Laterality comparison for the AD in 3T',
           output_fig = os.path.join(git_directory, 'figures', f'{study_group_name}_AD_3T.png'),
           bonferroni = bonferroni)
afq_figure(AFQ_L_FA_7T[o:d], AFQ_R_FA_7T[o:d],
           suptitle = 'Laterality comparison for the FA in 7T',
           output_fig = os.path.join(git_directory, 'figures', f'{study_group_name}_FA_7T.png'),
           bonferroni = bonferroni)
afq_figure(AFQ_L_MD_7T[o:d], AFQ_R_MD_7T[o:d],
           suptitle = 'Laterality comparison for the MD in 7T',
           output_fig = os.path.join(git_directory, 'figures', f'{study_group_name}_MD_7T.png'),
           bonferroni = bonferroni)
afq_figure(AFQ_L_RD_7T[o:d], AFQ_R_RD_7T[o:d],
           suptitle = 'Laterality comparison for the RD in 7T',
           output_fig = os.path.join(git_directory, 'figures', f'{study_group_name}_RD_7T.png'),
           bonferroni = bonferroni)
afq_figure(AFQ_L_AD_7T[o:d], AFQ_R_AD_7T[o:d],
           suptitle = 'Laterality comparison for the AD in 7T',
           output_fig = os.path.join(git_directory, 'figures', f'{study_group_name}_AD_7T.png'),
           bonferroni = bonferroni)

#%% Step 3: ML Classificator
y = [0]*len(study_group) + [1]*len(study_group)
itera = 100
# Laterality X matrices
LAT_X_FA_3T = np.concatenate((AFQ_L_FA_3T[o:d], AFQ_R_FA_3T[o:d]), axis = 1)
LAT_X_MD_3T = np.concatenate((AFQ_L_MD_3T[o:d], AFQ_R_MD_3T[o:d]), axis = 1)
LAT_X_RD_3T = np.concatenate((AFQ_L_RD_3T[o:d], AFQ_R_RD_3T[o:d]), axis = 1)
LAT_X_AD_3T = np.concatenate((AFQ_L_AD_3T[o:d], AFQ_R_AD_3T[o:d]), axis = 1)
LAT_X_FA_7T = np.concatenate((AFQ_L_FA_7T[o:d], AFQ_R_FA_7T[o:d]), axis = 1)
LAT_X_MD_7T = np.concatenate((AFQ_L_MD_7T[o:d], AFQ_R_MD_7T[o:d]), axis = 1)
LAT_X_RD_7T = np.concatenate((AFQ_L_RD_7T[o:d], AFQ_R_RD_7T[o:d]), axis = 1)
LAT_X_AD_7T = np.concatenate((AFQ_L_AD_7T[o:d], AFQ_R_AD_7T[o:d]), axis = 1)

print('\nFA 3T LAT  ---------------------------------------------------------')
ML_class(LAT_X_FA_3T.T, y,
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_FA_3T') 
print('\nMD 3T LAT  ---------------------------------------------------------')
ML_class(LAT_X_MD_3T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_MD_3T') 
print('\nRD 3T LAT  ---------------------------------------------------------')
ML_class(LAT_X_RD_3T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_RD_3T') 
print('\nAD 3T LAT  ---------------------------------------------------------')
ML_class(LAT_X_AD_3T.T, y,
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_AD_3T') 
print('\nFA 7T LAT  ---------------------------------------------------------')
ML_class(LAT_X_FA_7T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_FA_7T')
print('\nMD 7T LAT  ---------------------------------------------------------')
ML_class(LAT_X_MD_7T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_MD_7T')
print('\nRD 7T LAT  ---------------------------------------------------------')
ML_class(LAT_X_RD_7T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_RD_7T')
print('\nAD 7T LAT  ---------------------------------------------------------')
ML_class(LAT_X_AD_7T.T, y, 
         figures_outputdir = os.path.join(git_directory, 'figures'),
         iters = itera, fname = 'LAT_X_AD_7T') 

#%% COMBINATED MODELS

print('\nCOMBINED MODEL IN 3T -----------------------------------------------')
L = np.concatenate((AFQ_L_FA_3T[o:d], AFQ_L_MD_3T[o:d], AFQ_L_RD_3T[o:d],
                    AFQ_L_AD_3T[o:d]), axis = 0)
R = np.concatenate((AFQ_R_FA_3T[o:d], AFQ_R_MD_3T[o:d], AFQ_R_RD_3T[o:d],
                    AFQ_R_AD_3T[o:d]), axis = 0)
X = np.concatenate((L, R), axis = 1)
ML_class(X.T, y, iters = itera, fname = 'LAT_X_COMB_3T')

print('\nCOMBINED MODEL IN 7T -----------------------------------------------')
L = np.concatenate((AFQ_L_FA_7T[o:d], AFQ_L_MD_7T[o:d], AFQ_L_RD_7T[o:d],
                    AFQ_L_AD_7T[o:d]), axis = 0)
R = np.concatenate((AFQ_R_FA_7T[o:d], AFQ_R_MD_7T[o:d], AFQ_R_RD_7T[o:d],
                    AFQ_R_AD_7T[o:d]), axis = 0)
X = np.concatenate((L, R), axis = 1)
ML_class(X.T, y, iters = itera, fname = 'LAT_X_COMB_7T')