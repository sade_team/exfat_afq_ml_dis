#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 18:57:36 2020

@author: spascual
"""
#%% step 1: Global Analysis

def mean_value_calculator(TCK_IN, REFERENCE, MICRO_MAP, out_print = False):
    import random
    import string
    import os
    import nipype.interfaces.mrtrix3 as mrt
    import nipype.interfaces.fsl as fsl
    import shutil
    
    TMP_local = '/tmp/LSNA_' + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(16))
    
    if not os.path.isdir(TMP_local):
        os.makedirs(TMP_local)
    
    tdi = mrt.ComputeTDI()
    tdi.inputs.in_file = TCK_IN
    tdi.inputs.reference = REFERENCE
    tdi.inputs.args = "-quiet"
    tdi.inputs.out_file = os.path.join(TMP_local, 'tmp.nii.gz')
    if out_print: print('Running command: ' + tdi.cmdline)
    tdi.run()
    
    maths = fsl.MultiImageMaths()
    maths.inputs.in_file = os.path.join(TMP_local, 'tmp.nii.gz')
    maths.inputs.op_string = "-bin -mul %s"
    maths.inputs.operand_files = MICRO_MAP
    maths.inputs.out_file = os.path.join(TMP_local, 'microstructural_data.nii.gz')
    if out_print: print('Running command: ' + maths.cmdline)
    maths.run()

    fsl_stats = fsl.ImageStats()
    fsl_stats.inputs.in_file = os.path.join(TMP_local, 'microstructural_data.nii.gz')
    fsl_stats.inputs.op_string = '-M'
    if out_print: print('Running command: ' + fsl_stats.cmdline)
    stats_output = fsl_stats.run().outputs
    output = float(str(stats_output).split()[2])
    
    #REMOVING TEMPORAL FILE
    if os.path.isdir(TMP_local):
        shutil.rmtree(TMP_local)
    
    return output

def global_stat(path, field = '3T', out_print = False):
    import os
    import random
    import string
    import shutil
    
    ## OUTPUT VARIABLES:
    L_FA = []
    R_FA = []
    L_MD = []
    R_MD = []
    L_AD = []
    R_AD = []
    L_RD = []
    R_RD = []
    
    for idx, subject in enumerate(sorted(os.listdir(path))): 
        if os.path.isdir(os.path.join(path, subject)):
            tck_file_L = os.path.join(path, subject, 'diffusion_' + field + '/exFAT_L_10M.tck')
            tck_file_R = os.path.join(path, subject, 'diffusion_' + field + '/exFAT_R_10M.tck')
            microstuc_map_FA = os.path.join(path, subject, 'diffusion_' + field + '/dti_FA.nii.gz')
            microstuc_map_MD = os.path.join(path, subject, 'diffusion_' + field + '/dti_MD.nii.gz')
            microstuc_map_AD = os.path.join(path, subject, 'diffusion_' + field + '/dti_AD.nii.gz')
            microstuc_map_RD = os.path.join(path, subject, 'diffusion_' + field + '/dti_RD.nii.gz')
            
            ## NEEDED FILES:
            skip_subject = False
            needed_files = []
            needed_files.append(tck_file_L)
            needed_files.append(tck_file_R)
            needed_files.append(microstuc_map_FA)
            needed_files.append(microstuc_map_MD)
            needed_files.append(microstuc_map_AD)
            needed_files.append(microstuc_map_RD)
            
            for needed_file in needed_files:
                if not os.path.isfile(needed_file):
                    skip_subject = True
                    
            if skip_subject:
                continue
            
            TMP = '/tmp/LSNA_' + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(16))
            if not os.path.isdir(TMP):
                os.makedirs(TMP)
            
            if field == '3T':
                dwi_file = os.path.join(path, subject, 'diffusion_3T', 'DWI.mif')
            elif field == '7T':
                dwi_file = '/home/spascual/data/lsna_2/HCP_7T_MSMTCSD-OUTPUT/' + subject + '/DWI.mif'
            
            L_FA.append(mean_value_calculator(TCK_IN = tck_file_L, REFERENCE = dwi_file, MICRO_MAP = microstuc_map_FA))
            R_FA.append(mean_value_calculator(TCK_IN = tck_file_R, REFERENCE = dwi_file, MICRO_MAP = microstuc_map_FA))
            L_MD.append(mean_value_calculator(TCK_IN = tck_file_L, REFERENCE = dwi_file, MICRO_MAP = microstuc_map_MD))
            R_MD.append(mean_value_calculator(TCK_IN = tck_file_R, REFERENCE = dwi_file, MICRO_MAP = microstuc_map_MD))
            L_AD.append(mean_value_calculator(TCK_IN = tck_file_L, REFERENCE = dwi_file, MICRO_MAP = microstuc_map_AD))
            R_AD.append(mean_value_calculator(TCK_IN = tck_file_R, REFERENCE = dwi_file, MICRO_MAP = microstuc_map_AD))
            L_RD.append(mean_value_calculator(TCK_IN = tck_file_L, REFERENCE = dwi_file, MICRO_MAP = microstuc_map_RD))
            R_RD.append(mean_value_calculator(TCK_IN = tck_file_R, REFERENCE = dwi_file, MICRO_MAP = microstuc_map_RD))

            ## REMOVING TEMPORAL FILE
            if os.path.isdir(TMP):
                shutil.rmtree(TMP)

    return L_FA, R_FA, L_MD, R_MD, L_AD, R_AD, L_RD, R_RD

#%% step 2: AFQ Analysis

def afq_figure(H1, H2, output_fig, suptitle = 'SUPERIOR TITLE', l = 0, r = 100,
           p1_title = 'Left pathway', p2_title = 'Right pathway',
           p3_title = 'Laterality index', bonferroni = 1, figs = True):
    
    ## Local libraries
    import matplotlib.pyplot as plt
    import matplotlib.gridspec as gridspec
    from scipy import stats
    import numpy as np
    import os
    
    print('\n' + suptitle)
    
    ## Figure grid
    gs = gridspec.GridSpec(1, 3, wspace = 0.33)
    plt.figure(figsize=(10, 2), dpi=300)
    ax1 = plt.subplot(gs[0,0])
    ax1.set_title(p1_title)
    ax2 = plt.subplot(gs[0,1])
    ax2.set_title(p2_title)
    ax3 = plt.subplot(gs[0,2])
    ax3.set_title(p3_title)
    
    ax1.grid(which = 'both')
    ax2.grid(which = 'both')
    ax3.grid(which = 'both')
    
    
    
    # Mean signals
    H1_mean = np.mean(H1, axis = 1)
    H2_mean = np.mean(H2, axis = 1)
    LI_mean = (np.mean(H1, axis = 1) - np.mean(H2, axis = 1))/(np.mean(H1, axis = 1) + np.mean(H2, axis = 1))

    ## Parametric statistics
    p_val = np.zeros((np.shape(H1)[0], 1))
    ground = np.zeros((np.shape(H1)[0], 1))

    for node in range(0, len(p_val)):
        _, p_val[node] = stats.ttest_ind(H1[node, :], H2[node, :])
        if p_val[node] * bonferroni > 0.05:
            ground[node] = 100
    p_val = p_val[l:r]

    if len(p_val[p_val * bonferroni < 0.05]) != 0:
        LI_part = LI_mean[l:r]
        GR_part = np.transpose(ground[l:r])[0]
        ax3.fill_between(range(0,len(LI_part)), 0, LI_part, facecolor = 'rebeccapurple', where = abs(LI_part) > GR_part, zorder = 1)
        p_max = np.max(p_val[p_val * bonferroni < 0.05]) * bonferroni
               
        if p_max >= 0.001:
            ax3.legend(('$p_{max}$ ($B_{corr}$) = ' + str(np.round(p_max, 3)), '_', '_'), shadow = True, loc = (.15, -.40), handlelength = 1.5, fontsize = 10)
        else:
            ax3.legend(('$p_{max}$ ($B_{corr}$) < 0.001 ' , '_', '_'), shadow = True, loc = (.15, -.40), handlelength = 1.5, fontsize = 10)

    ## Case signals
    for x in range(0, np.shape(H1)[1]): ax1.plot(H1[l:r, x], color = 'lightsalmon', zorder = -1, linewidth = 0.2)
    for x in range(0, np.shape(H2)[1]): ax2.plot(H2[l:r, x], color = 'lightsalmon', zorder = -1, linewidth = 0.2)
    # LI cases
    #for x in range(0, np.min([np.shape(H1)[1], np.shape(H2)[1]])): ax3.plot((H1[l:r, x] - H2[l:r, x])/(H1[l:r, x] + H2[l:r, x]), color = 'lightsalmon', zorder = -1)

    ## Box color    
    plt.setp(ax1, fc = (0.95, 0.95, 0.95))
    plt.setp(ax2, fc = (0.95, 0.95, 0.95))
    plt.setp(ax3, fc = (0.95, 0.95, 0.95))
    
    ## Mean representation
    ax1.plot(H1_mean[l:r], color = 'darkred', linewidth=1.5)
    ax2.plot(H2_mean[l:r], color = 'darkred', linewidth=1.5)
    ax3.plot(LI_mean[l:r], color = 'darkred', linewidth=1.5)
    ax3.axhline(linewidth=1, color='black')
    plt.yticks(np.arange(-0.04, 0.05, 0.02))
    if figs: plt.savefig(output_fig, format= 'png', bbox_inches="tight")
    plt.show()
    
    AUC_0 = 0
    AUC_f = 0
    RESULT = False
    for i in range(len(ground)):
        if ground[i] == 0:
            if not RESULT:
                RESULT = True
                AUC_0 = i
            else:
                AUC_f = i
        elif ground[i] == 100:
            if RESULT:
                print('Significant result found (max p_bonferroni = %.3f) found from %i to %i with AUC %f.' % (np.round(np.max(p_val[AUC_0:AUC_f+1])*bonferroni, 3), AUC_0+1, AUC_f+1, np.absolute(np.sum(LI_mean[AUC_0:AUC_f+1]))))
                RESULT = False
        if i == 99:
            if RESULT:
                if AUC_0 == i:
                    print('Significant result found (max p_bonferroni = %.3f) found in %i with AUC %f.' % (np.round(np.max(p_val[i])*bonferroni, 3), i+1, np.absolute(np.sum(LI_mean[i]))))
                else:
                    print('Significant result found (max p_bonferroni = %.3f) found from %i to %i with AUC %f.' % (np.round(np.max(p_val[AUC_0:AUC_f+1])*bonferroni, 3), AUC_0+1, AUC_f+1, np.absolute(np.sum(LI_mean[AUC_0:AUC_f+1]))))
                    RESULT = False

#%% step 3: ML Functions
def ML_class(X, y, figures_outputdir, iters = 2000, fname = 'test'):
    from scipy.stats import zscore
    import os
    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn.model_selection import train_test_split
    import seaborn as sns
    from sklearn.linear_model import LogisticRegression
      
    print('\nRunning %i iters random iterations...' % iters)
    score = np.zeros([iters])
    
    for e in range(iters):  # repetitions loop
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
        X_train = zscore(X_train)
        X_test = zscore(X_test)
        
        model = LogisticRegression(solver="lbfgs", penalty="l2", max_iter=200)
        model.fit(X_train, y_train)
        score[e] = model.score(X_test, y_test)
    
    print('\nInstantiate a model for random guessing')
    score_rnd = np.zeros([iters])
    for e in range(iters):
        y_rnd = np.random.permutation(y)
        model_rnd = LogisticRegression(solver = "lbfgs", penalty = "l2", max_iter = 200)
        X_rnd_train, X_rnd_test, y_rnd_train, y_rnd_test = train_test_split(X, y_rnd, test_size = 0.2)
        model_rnd.fit(X_rnd_train, y_rnd_train)
        score_rnd[e] = model.score(X_rnd_test, y_rnd_test)

    print('\nPlot comparison as violin plots')
    fig, ax = plt.subplots(nrows = 1, ncols = 1, sharex = True)
    sns.violinplot(data=[score, score_rnd], cut=0, orient='h', scale='width', ax=ax, bw=.9)
    ax.set_yticklabels(['LR', 'random'])
    ax.set_xlabel('test accuracy')
    ax.axvline(0.5, linestyle='dashed', color='black')
    ax.axvline(score.mean(), linestyle='dashed', color='black')
    
    plt.savefig(os.path.join(figures_outputdir, fname + '.eps'), format = 'eps', bbox_inches="tight")
    plt.show()
    
    print("Test accuracy: %.3f +/- %.3f (mean +/- standard deviation)" %(score.mean(), score.std()))
    return

def vector_comparison(X, y):
    from scipy import stats
    
    column_a_values = []
    column_b_values = []
    for i in range(len(y)):
        if y[i] == 1:
            column_a_values.append(X[i])
        if y[i] == 0:
            column_b_values.append(X[i])
    _, p = stats.ttest_ind(column_a_values, column_b_values)
    return p
