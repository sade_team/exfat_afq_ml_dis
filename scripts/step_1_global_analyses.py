#!/usr/bin/env python3
# -*- coding: utf-8 -*-

main_dir = '/home/lsna/DATA'
git_directory = '/home/lsna/git/exfat_afq_ml_dis/'
figures_dir = git_directory + 'figures'

#%% DO NOT MODIFY BEYOND THIS POINT
__author__ = "Saul Pascual-Diaz, Federico Varriano"
__institution__ = "Laboratory of Surgical Neuroanatomy (LSNA)."
__date__ = "2020/06/24"
__version__ = "1.1"
__status__ = "paper_version"

#%% Script info
'''
This script is used to obtain the mean value for the global exFAT for the 
diffusion-derived maps.

FILE STRUCTURE:
    tck_file_L = project_dir/subject/diffusion_ + field/exFAT_L_10M.tck)
    tck_file_R = project_dir/subject/diffusion_ + field/exFAT_R_10M.tck)
    microstuc_map_FA = project_dir/subject/diffusion_ + field/dti_FA.nii.gz)
    microstuc_map_MD = project_dir/subject/diffusion_ + field/dti_MD.nii.gz)
    microstuc_map_AD = project_dir/subject/diffusion_ + field/dti_AD.nii.gz)
    microstuc_map_RD = project_dir/subject/diffusion_ + field/dti_RD.nii.gz')
    Where field is the string '3T' or '7T'.

The output is stored in <path>/git/exfat_afq_ml_dis/global_output
'''

#%% Loading Libraries
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
import os
os.chdir(os.path.join(git_directory, 'scripts'))
from lsna_dependences import global_stat

sns.set(style="whitegrid")

#%% Do work

if not os.path.isfile(os.path.join(git_directory, 'global_output', 'L_FA_7T.npy')):
    L_FA_3T, R_FA_3T, L_MD_3T, R_MD_3T, L_AD_3T, R_AD_3T, L_RD_3T, R_RD_3T = global_stat(main_dir, field = '3T')
    L_FA_7T, R_FA_7T, L_MD_7T, R_MD_7T, L_AD_7T, R_AD_7T, L_RD_7T, R_RD_7T = global_stat(main_dir, field = '7T')
    np.save(os.path.join(git_directory, 'global_output', 'L_FA_3T.npy'), np.array(L_FA_3T[:120]))
    np.save(os.path.join(git_directory, 'global_output', 'R_FA_3T.npy'), np.array(R_FA_3T[:120]))
    np.save(os.path.join(git_directory, 'global_output', 'L_MD_3T.npy'), np.array(L_MD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_MD_3T.npy'), np.array(R_MD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'L_RD_3T.npy'), np.array(L_RD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_RD_3T.npy'), np.array(R_RD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'L_AD_3T.npy'), np.array(L_AD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_AD_3T.npy'), np.array(R_AD_3T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'L_FA_7T.npy'), np.array(L_FA_7T[:120]))
    np.save(os.path.join(git_directory, 'global_output', 'R_FA_7T.npy'), np.array(R_FA_7T[:120]))
    np.save(os.path.join(git_directory, 'global_output', 'L_MD_7T.npy'), np.array(L_MD_7T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_MD_7T.npy'), np.array(R_MD_7T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'L_RD_7T.npy'), np.array(L_RD_7T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_RD_7T.npy'), np.array(R_RD_7T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'L_AD_7T.npy'), np.array(L_AD_7T[:120])*1000)
    np.save(os.path.join(git_directory, 'global_output', 'R_AD_7T.npy'), np.array(R_AD_7T[:120])*1000)
else:
    L_FA_3T = np.load(os.path.join(git_directory, 'global_output', 'L_FA_3T.npy'))
    R_FA_3T = np.load(os.path.join(git_directory, 'global_output', 'R_FA_3T.npy'))
    L_MD_3T = np.load(os.path.join(git_directory, 'global_output', 'L_MD_3T.npy'))
    R_MD_3T = np.load(os.path.join(git_directory, 'global_output', 'R_MD_3T.npy'))
    L_RD_3T = np.load(os.path.join(git_directory, 'global_output', 'L_RD_3T.npy'))
    R_RD_3T = np.load(os.path.join(git_directory, 'global_output', 'R_RD_3T.npy'))
    L_AD_3T = np.load(os.path.join(git_directory, 'global_output', 'L_AD_3T.npy'))
    R_AD_3T = np.load(os.path.join(git_directory, 'global_output', 'R_AD_3T.npy'))
    L_FA_7T = np.load(os.path.join(git_directory, 'global_output', 'L_FA_7T.npy'))
    R_FA_7T = np.load(os.path.join(git_directory, 'global_output', 'R_FA_7T.npy'))
    L_MD_7T = np.load(os.path.join(git_directory, 'global_output', 'L_MD_7T.npy'))
    R_MD_7T = np.load(os.path.join(git_directory, 'global_output', 'R_MD_7T.npy'))
    L_RD_7T = np.load(os.path.join(git_directory, 'global_output', 'L_RD_7T.npy'))
    R_RD_7T = np.load(os.path.join(git_directory, 'global_output', 'R_RD_7T.npy'))
    L_AD_7T = np.load(os.path.join(git_directory, 'global_output', 'L_AD_7T.npy'))
    R_AD_7T = np.load(os.path.join(git_directory, 'global_output', 'R_AD_7T.npy'))

#%% Creating dataframe
    
columns = ['Value', 'Field', 'Diffusion parameter', 'Hemisphere']
dataframe = pd.DataFrame(columns = columns)

for i in range(0, 100):
    data_list = []
    data_columns = []
    data_list.append(L_FA_3T[i])
    data_list.append('FA')
    data_list.append('3T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_FA_3T[i])
    data_list.append('FA')
    data_list.append('3T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_MD_3T[i])
    data_list.append('MD')
    data_list.append('3T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_MD_3T[i])
    data_list.append('MD')
    data_list.append('3T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_RD_3T[i])
    data_list.append('RD')
    data_list.append('3T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_RD_3T[i])
    data_list.append('RD')
    data_list.append('3T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_AD_3T[i])
    data_list.append('AD')
    data_list.append('3T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_AD_3T[i])
    data_list.append('AD')
    data_list.append('3T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_FA_7T[i])
    data_list.append('FA')
    data_list.append('7T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_FA_7T[i])
    data_list.append('FA')
    data_list.append('7T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_MD_7T[i])
    data_list.append('MD')
    data_list.append('7T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_MD_7T[i])
    data_list.append('MD')
    data_list.append('7T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_RD_7T[i])
    data_list.append('RD')
    data_list.append('7T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_RD_7T[i])
    data_list.append('RD')
    data_list.append('7T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(L_AD_7T[i])
    data_list.append('AD')
    data_list.append('7T')
    data_list.append('Left')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False)
    
    data_list = []
    data_columns = []
    data_list.append(R_AD_7T[i])
    data_list.append('AD')
    data_list.append('7T')
    data_list.append('Right')
    data_columns.append('Value')
    data_columns.append('Diffusion parameter')
    data_columns.append('Field')
    data_columns.append('Hemisphere')
    tmp_dataframe = pd.DataFrame(data = [data_list], columns = data_columns)
    dataframe = pd.concat([dataframe, tmp_dataframe], sort = False) 

#%% Plot
sns.catplot(x="Diffusion parameter", y="Value", hue="Hemisphere", col="Field",
            data=dataframe, kind="violin", split=True, height=8, aspect=.7,
            inner="quartile", palette="Set2");
plt.savefig(os.path.join(git_directory, 'figures', 'global_lat_comparison.eps'),
            format = 'eps')

#%% Statistics
p_values = np.zeros((8,1))
t_values = np.zeros((8,1))
bonferroni = 808

t_values[0], p_values[0] = stats.ttest_ind(L_FA_3T, R_FA_3T)
t_values[1], p_values[1] = stats.ttest_ind(L_MD_3T, R_MD_3T)
t_values[2], p_values[2] = stats.ttest_ind(L_RD_3T, R_RD_3T)
t_values[3], p_values[3] = stats.ttest_ind(L_AD_3T, R_AD_3T)
t_values[4], p_values[4] = stats.ttest_ind(L_FA_7T, R_FA_7T)
t_values[5], p_values[5] = stats.ttest_ind(L_MD_7T, R_MD_7T)
t_values[6], p_values[6] = stats.ttest_ind(L_RD_7T, R_RD_7T)
t_values[7], p_values[7] = stats.ttest_ind(L_AD_7T, R_AD_7T)

p_values = p_values*bonferroni
print('\nOUTPUT\n-------------')
print(p_values < 0.05)
print(p_values[p_values < 0.05])

#%% prints for LaTeX
print(f'3T left & ${np.round(L_FA_3T.mean(), 3)} \pm {np.round(L_FA_3T.std(), 3)}$ & ${np.round(L_MD_3T.mean(), 3)} \pm {np.round(L_MD_3T.std(), 3)}$ & ${np.round(L_RD_3T.mean(), 3)} \pm {np.round(L_RD_3T.std(), 3)}$ & ${np.round(L_AD_3T.mean(), 3)} \pm {np.round(L_AD_3T.std(), 3)}$ \\')
print(f'3T right & ${np.round(R_FA_3T.mean(), 3)} \pm {np.round(R_FA_3T.std(), 3)}$ & ${np.round(R_MD_3T.mean(), 3)} \pm {np.round(R_MD_3T.std(), 3)}$ & ${np.round(R_RD_3T.mean(), 3)} \pm {np.round(R_RD_3T.std(), 3)}$ & ${np.round(R_AD_3T.mean(), 3)} \pm {np.round(R_AD_3T.std(), 3)}$ \\')
print(f'7T left & ${np.round(L_FA_7T.mean(), 3)} \pm {np.round(L_FA_7T.std(), 3)}$ & ${np.round(L_MD_7T.mean(), 3)} \pm {np.round(L_MD_7T.std(), 3)}$ & ${np.round(L_RD_7T.mean(), 3)} \pm {np.round(L_RD_7T.std(), 3)}$ & ${np.round(L_AD_7T.mean(), 3)} \pm {np.round(L_AD_7T.std(), 3)}$ \\')
print(f'7T right & ${np.round(R_FA_7T.mean(), 3)} \pm {np.round(R_FA_7T.std(), 3)}$ & ${np.round(R_MD_7T.mean(), 3)} \pm {np.round(R_MD_7T.std(), 3)}$ & ${np.round(R_RD_7T.mean(), 3)} \pm {np.round(R_RD_7T.std(), 3)}$ & ${np.round(R_AD_7T.mean(), 3)} \pm {np.round(R_AD_7T.std(), 3)}$ \\')    
